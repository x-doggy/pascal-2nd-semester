{$mode objfpc}
{$R-}

unit matrix_unit;

interface

type
    TInt = integer;
    TLong = longint;
    TElem = single;
    TIntArr = array[1..1] of TInt;
    PIntArr = ^TIntArr;
    PMatrix = ^Matrix;
    TArr = array[1..1] of TElem;
    PArr = ^TArr;
    TMas = array[1..1] of PArr;
    PMas = ^TMas;
    Matrix = object
        private
            low1, high1,
            low2, high2,
            lines, coloumns: TInt;
            massive: PMas;
            procedure fatalError( ce: TInt );
        public
            constructor create( l1, h1, l2, h2: TInt );
            destructor destroy;
            procedure setElem( i1, i2: TInt; elem: TElem );
            procedure getElem( i1, i2: TInt; var elem: TElem );
            function getLowCol: TInt;
            function getHighCol: TInt;
            function getLowRow: TInt;
            function getHighRow: TInt;
            function getColCount: TInt;
            function getRowCount: TInt;
            procedure shellSort;
    end;

implementation

{Создаётся матрица с диапазонами индексов L1..H1 и L2..H2
(т.е. L1, H1, L2, H2 - входные параметры конструктора).
Т.е. выделяется память под двумерный динамический массив
размерами (H1-L1) на (H2-L2) и заполняются все поля объекта.
Если память выделить невозможно, то вызвается процедура обработки ошибки.}
constructor Matrix.create( l1, h1, l2, h2: TInt );
var need1, need2: TLong; i: TInt;
begin
    if (l1 > h1) or (l2 > h2) then fatalError(1)
    else begin
        coloumns := ord(h1) - ord(l1) + 1;
        lines := ord(h2) - ord(l2) + 1;
        low1 := l1; high1 := h1;
        low2 := l2; high2 := h2;
        need1 := longint(coloumns) * sizeof(PArr);
        getmem(massive, need1);
        need2 := longint(lines) * sizeof(TElem);
        for i:=1 to coloumns do getmem(massive^[i], need2);
    end;
end;

{Уничтожается матрица (деструктор).
Т.е. освобождается память от массива и присваивается
всем полям нулевые значения.}
destructor Matrix.destroy;
var need1, need2: TLong; i: TInt;
begin
    need1 := longint(coloumns) * sizeof(PArr);
    need2 := longint(lines) * sizeof(TElem);
    for i:=1 to coloumns do freemem(massive^[i], need2);
    freemem(massive, need1);
    //massive := NIL;
    low1 := 0; high1 := 0;
    low2 := 0; high2 := 0;
    lines := 0; coloumns := 0;
end;

// Процедурой изменяется элемент с индексами i1, i2
procedure Matrix.setElem( i1, i2: TInt; elem: TElem );
var ind1, ind2: TInt;
begin
    if (i1 > high1) or (i1 < low1) then fatalError(3)
    else if (i2 > high2) or (i2 < low2) then fatalError(4)
    else begin
        ind1 := ord(i1) - ord(low1) + 1;
        ind2 := ord(i2) - ord(low2) + 1;
        massive^[ind1]^[ind2] := elem;
    end;
end;

// Процедурой возвращается элемент с индексами i1, i2
procedure Matrix.getElem( i1, i2: TInt; var elem: TElem );
var ind1, ind2: TInt;
begin
    if (i1 > high1) or (i1 < low1) then fatalError(5)
    else if (i2 > high2) or (i2 < low2) then fatalError(6)
    else begin
        ind1 := ord(i1) - ord(low1) + 1;
        ind2 := ord(i2) - ord(low2) + 1;
        elem := massive^[ind1]^[ind2];
    end;
end;

// Функцией возвращается нижняя граница индексов столбцов
function Matrix.getLowCol: TInt;
begin
    getLowCol := low1;
end;

// Функцией возвращается верхняя граница индексов столбцов
function Matrix.getHighCol: TInt;
begin
    getHighCol := high1;
end;

// Функцией возвращается нижняя граница индексов строк
function Matrix.getLowRow: TInt;
begin
    getLowRow := low2;
end;

// Функцией возвращается верхняя граница индексов строк
function Matrix.getHighRow: TInt;
begin
    getHighRow := high2;
end;

// Функцией возвращается число столбцов
function Matrix.getColCount: TInt;
begin
    getColCount := coloumns;
end;

// Функцией возвращается число строк
function Matrix.getRowCount: TInt;
begin
    getRowCount := lines;
end;

// Процедура выполняет сортировку Шелла со столбцам матрицы
procedure Matrix.shellSort;
var i, j, k, need1, need2: TLong;
    b, h: PIntArr;
    x, m, t, s, cols: TInt;
    buf: {PArr} pointer;
begin
    if (massive<>NIL) then begin
        cols := getColCount;

        need1 := longint(cols) * sizeof(TInt);
        getmem(b, need1);

        // Формирование массива ключей
        for i:=getLowCol to getHighCol do begin
            s := 0;
            for j:=getLowRow to getHighRow do
                if massive^[i]^[j] < 0 then s := s + 1;
            b^[i] := s;
        end;

       { writeln('Массив ключей (до):');
        for i:=1 to cols do write( b^[i], ', ' );
        writeln; }

        // Сама суть сортировки
        t := trunc( ln(cols) / ln(2) ) - 1;
        need2 := longint(t) * sizeof(TInt);
        getmem(h, need2);
        h^[t] := 1;

        for m:=t-1 downto 1 do h^[m] := 2 * h^[m+1] + 1;
        for m:=1 to t do begin
            k := h^[m];
            for i:=k+1 to cols do begin
                x := b^[i];
                j := i-k;
                buf := massive^[i];
                while (j>0) and (x <= b^[j]) do begin
                    b^[j+k] := b^[j];
                    massive^[j+k] := massive^[j];
                    j := j - k;
                end;
                b^[j+k] := x;
                massive^[j+k] := buf;
            end;
        end;

        {writeln('Массив ключей (после):');
        for i:=1 to cols do write( b^[i], ', ' );
        writeln; writeln;

        for i:=getLowRow to getHighRow do begin
            for j:=getLowCol to getHighCol do
                 write(massive^[j]^[i]:10:2);
             writeln;
        end;}

        freemem(h, need2);
        h := NIL;
        freemem(b, need1);
        b := NIL;
    end else fatalError(7);
end;

{Процедурой обрабатываются ошибки (фатально).
* Входной параметр - номер ошибки.
* Выводит на экран описание возникшей ошибки и завершает работу
* всей программы с помошью halt(1)}
procedure Matrix.fatalError(ce: TInt);
begin
    case ce of
    1: writeln('Были заданы неверные границы массива');
    2: writeln('Был передан пустой массив');
    3: writeln('Неверные нижние грани при записи элемента в матрицу');
    4: writeln('Неверные верхние грани при записи элемента в матрицу');
    5: writeln('Неверные нижние грани при получении элемента из матрицы');
    6: writeln('Неверные верхние грани при получении элемента из матрицы');
    7: writeln('Не создан массив, который должен быть отсортирован');
    end;
    halt(1);
end;

begin {initialization}
    {$R+}
end.
