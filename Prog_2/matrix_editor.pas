program matrix_editor;
uses crt, matrix_unit;
const MAX_Y = 17;
      COL_LENGTH = 16;
      cBlack = 0;
      cGray = 7;
      cGreen = 2;
var i, j, colcount,
    X0,Y0,X1,Y1, cols, lines,
    m, n: TInt;
    a: PMatrix;
    elem: TElem;
    wKey: char;
    diff: byte;
    inlist: boolean;
    
{
Редактор.
MAX_Y = кол-во строк на одной экране
COL_LENGTH = формат числа для колонки
}

begin
    clrscr;

    write('Введите кол-во столбцов: ');
    readln(n);
    write('Введите кол-во строк: ');
    readln(m);

    if (m>0) or (n>0) then new( a, create(1, n, 1, m) );
    for i:=a^.getLowRow to a^.getHighRow do
        for j:=a^.getLowCol to a^.getHighCol do
          a^.setElem( j, i, 0 );

    clrscr;

    X0:=1;
    Y0:=1;
    X1:=1;
    Y1:=1;
    diff:=1;

    repeat
        textcolor(cGray);
        colcount := trunc(74/COL_LENGTH);
        GotoXY(6, 1);
        write('|');
        if diff=2 then clrscr;
        for i:=1 to MAX_Y do begin
            GotoXY(1, i+1);
            write(i+Y1-1);
            GotoXY(6, i+1);
            write('|');
        end;
        for i:=1 to colcount do begin
            gotoxy(5+((i-1)*COL_LENGTH)+i, 1);
            write('|',i+X1-1);
        end;
        for j:=1 to colcount do
            for i:=1 to MAX_Y do begin
                gotoxy(5+((j-1)*COL_LENGTH)+j, i+1);
                write('|');
            end;

        cols  := a^.getColCount;
        lines := a^.getRowCount;
        if colcount>cols then m:=cols else m:=colcount;
        if lines<MAX_Y then n:=lines else n:=MAX_Y;
        for i:=1 to m do
            for j:=1 to n do begin
                gotoxy(6+((i-1)*COL_LENGTH)+i,j+1);
                if ((j+Y1-1)<=lines) and ((i+X1-1)<=cols) then begin
                    if (i=X0) and (j=Y0) then textcolor(cGreen) else textcolor(cGray);
                    //a^.getElem(j+Y1-1,i+X1-1,elem);
                    a^.getElem(i+X1-1, j+Y1-1, elem);
                    write(elem);
                end;
            end;
        gotoxy(1, 21);
        write('[Enter]-изменить, [z],[x]-тип перелистывания, [s]-сортировка');
        inlist:=true;
        repeat
            if diff<>1 then inlist:=false;
            wKey := readkey;
            case wKey of
            #72: // стрелка вверх
            begin
                case diff of
                1: if (Y0>1) then begin
                    //a^.getElem(y0+y1-1,x0+x1-1,elem);
                    a^.getElem(X0+X1-1, Y0+Y1-1, elem);
                    textcolor(cGray);
                    gotoxy(6+((x0-1)*COL_LENGTH)+x0,y0+1);
                    write(elem);
                    Y0:=Y0-1;
                    //a^.getElem(y0+y1-1,x0+x1-1,elem);
                    a^.getElem(X0+X1-1, Y0+Y1-1, elem);
                    textcolor(cGreen);
                    gotoxy(6+((x0-1)*COL_LENGTH)+x0,y0+1);
                    write(elem);
                end;
                2: if (Y1>1) then Y1:=Y1-1;
                end;
            end;

            #75: // стрелка влево
            begin
                case diff of
                1:if (X0>1) then begin
                //a^.getElem(y0+y1-1,x0+x1-1,elem);
                a^.getElem(X0+X1-1, Y0+Y1-1,elem);
                textcolor(cGray);
                gotoxy(6+((x0-1)*COL_LENGTH)+x0,y0+1);
                write(elem);
                X0:=X0-1;
                //a^.getElem(y0+y1-1,x0+x1-1,elem);
                a^.getElem(X0+X1-1, Y0+Y1-1, elem);
                textcolor(cGreen);
                gotoxy(6+((x0-1)*COL_LENGTH)+x0,y0+1);
                write(elem);
              end;
                2: if (X1>1) then X1:=X1-1;
            end;
            end;

            #77: // стрелка вправо
            begin
                case diff of
                1: if (X0<colcount) and (x0<cols) then begin
                    //a^.getElem(y0+y1-1,x0+x1-1,elem);
                    a^.getElem(X0+X1-1, Y0+Y1-1, elem);
                    textcolor(cGray);
                    gotoxy(6+((x0-1)*COL_LENGTH)+x0,y0+1);
                    write(elem);
                    X0:=X0+1;
                    //a^.getElem(y0+y1-1,x0+x1-1,elem);
                    a^.getElem(X0+X1-1, Y0+Y1-1, elem);
                    textcolor(cGreen);
                    gotoxy(6+((x0-1)*COL_LENGTH)+x0,y0+1);
                    write(elem);
                end;
                2: if (x1<=cols-colcount) then X1:=X1+1;
                end;
            end;

            #80: // стрелка вниз
            begin
                case diff of
                1: if (Y0<MAX_Y) and (y0<lines) then begin
                    //a^.getElem(y0+y1-1,x0+x1-1,elem);
                    a^.getElem(X0+X1-1, Y0+Y1-1, elem);
                    textcolor(cGray);
                    gotoxy(6+((x0-1)*COL_LENGTH)+x0,y0+1);
                    write(elem);
                    Y0:=Y0+1;
                    //a^.getElem(y0+y1-1,x0+x1-1,elem);
                    a^.getElem(X0+X1-1, Y0+Y1-1, elem);
                    textcolor(cGreen);
                    gotoxy(6+((x0-1)*COL_LENGTH)+x0,y0+1);
                    write(elem);
                end;
                2: if (y1<=lines-MAX_Y) then Y1:=Y1+1;
                end;
            end;

            #13: // Клавиша [Enter]
            begin
                gotoXY(1,19);
                write('Значение элемента: ');
                {$I-} read(elem); {$I+}
                if IORESULT=0 then begin
                    gotoXY(1,19);
                    textcolor(cBlack);
                    write('Значение стало = ',elem);
                    //a^.setElem(y0+y1-1,x0+x1-1,elem);
                    a^.setElem(X0+X1-1, Y0+Y1-1, elem);
                    textcolor(cGreen);
                    gotoxy(6+((x0-1)*COL_LENGTH)+x0,y0+1);
                    write(elem);
                    textcolor(cGray);
                end;
            end;

            'z': if diff=1 then diff:=2 else diff:=1;

            'x': if diff=2 then diff:=1 else diff:=2;

            's':
            begin
                if a<>NIL then a^.shellSort;
                inlist:=false;
            end;
            end;

        until not inlist;
    until wKey=#27;
end.
