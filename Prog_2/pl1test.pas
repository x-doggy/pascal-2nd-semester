program pl1test;
uses crt, pl1list;
var L: PList1;
    i, length, key: word;
    elem: TElem;
    fname: string;
    f: file of TElem;

begin {main}

    L := NIL;

    repeat
        textcolor(11);
        clrscr;

        writeln(' (c) 2014, Владимир Стадник, 1-я лаба');
        writeln(' Программа написана в Lubuntu 14.04, amd64');
        writeln('      Программа работы с L1-списком');
        writeln;
        writeln('1. Создать L1-список');
        writeln('2. Заполнить L1-список автоматически');
        writeln('3. Добавить элемент в L1-список');
        writeln('4. Вывести информацию о L1-списке');
        writeln('7. Загрузить L1 в файл');
        writeln('8. Выгрузить L1 из файла');
        writeln('9. Выйти из программы');
        write('Введите номер действия: ');
        readln(key);

        case key of
        1:
            begin
                if L=NIL then begin
                    new(L, Init);
                    writeln('L1-список создан')
                end else begin
                    dispose(L, Done);
                    L := NIL;
                    writeln('L1-список удален');
                end;
            end;

        2:
            begin
                randomize;
                write('Введите кол-во элементов списка: ');
                readln(length);
                L^.ToBegin;
                for i:=1 to length do begin
                    elem := random(100)-49;
                    L^.Insert(elem);
                    writeln(i, '.    ', elem);
                    L^.ToNext;
                end;
            end;

        3:
            begin
                write('Вставляемый элемент: ');
                readln(elem);
                while not L^.IsEnd do L^.ToNext;
                L^.Insert(elem);
                L^.ToNext;
                inc(length);
            end;

        4:
            begin
                L^.ToBegin;
                writeln('==== Размерность L1 = ', length, ' ====');
                writeln('Элементы:');
                for i:=1 to length do begin
                    L^.Get(elem);
                    writeln(elem);
                    L^.ToNext;
                end;
            end;

        7:
            begin
                L^.ToBegin;
                write('Имя файла: ');
                readln(fname);
                assign(f, fname);
                {$I-} rewrite(f); {$I+}
                if IORESULT<>0 then write('Ошибка записи файла!')
                else begin
                    for i:=1 to length do begin
                        L^.Get(elem);
                        {$I-} write(f, elem); {$I+}
                        if IORESULT<>0
                        then writeln('Ошибка записи в файл')
                        else L^.ToNext;
                    end;
                end;
                close(f);
            end;

        8:
            begin
                if L<>NIL then begin
                    L^.MakeEmpty;
                    writeln('L1-список был очищен!');
                end else begin
                    new(L, Init);
                    writeln('L1-список теперь создан!');
                end;
                length := 0;
                write('Имя файла: ');
                readln(fname);
                assign(f, fname);
                {$I-} reset(f); {$I+}
                if IORESULT<>0 then write('Ошибка чтения файла')
                else begin
                    L^.ToBegin;
                    while not EOF(f) do begin
                        {$I-} read(f, elem); {$I+}
                        if IORESULT<>0
                        then writeln('Ошибка чтения элемента')
                        else begin
                            L^.Insert(elem);
                            L^.ToNext;
                            inc(length);
                            writeln(i, '.    ', elem);
                        end;
                    end;
                end;
                close(f);
            end;
        end;
        if key<>9 then readkey;
    until key=9;

end.
