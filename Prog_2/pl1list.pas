unit pl1list;

interface

type TElem = integer;
     PList1 = ^CList1;
     CList1 = object
         private
            pb, pt: pointer;
            ErrCode: byte;
            procedure SetError(mask: byte);
            procedure ClearError(mask: byte);
         public
            constructor Init;
            constructor Copy(pl1: Plist1);
            destructor Done;
            procedure MakeEmpty;
            function IsEmpty: boolean;
            procedure ToBegin;
            function IsEnd: boolean;
            procedure ToNext;
            procedure Insert(x: TElem);
            procedure Extract(var x: TElem);
            procedure Get(var x: TElem);
            procedure Put(x: TElem);
            procedure Delete;
            function GetError: byte;
     end;

implementation

type PBlock = ^TBlock;
     TBlock = record
         data: TElem;
         next: PBlock;
     end;

constructor CList1.Init;
var p: PBlock;
begin
    new(p);
    p^.next := p;
    pb := p;
    pt := p;
end;

destructor CList1.Done;
begin
    MakeEmpty;
    dispose(PBlock(pb));
end;

constructor CList1.Copy(pl1: PList1);
var p: PBlock;
    x: TElem;
begin
    if pl1=NIL then fail else begin
        new(p);
        pb := p;
        pt := p;
        while not pl1^.IsEnd do begin
              pl1^.Get(x);
              Insert(x);
              ToNext;
              pl1^.ToNext;
        end;
    end;
end;

function CList1.IsEmpty: boolean;
begin
    IsEmpty := ( PBLock(pb)^.next = pb );
end;

procedure CList1.MakeEmpty;
var p: PBlock;
begin
    if PBlock(pb)^.next<>pb then begin
        p := PBlock(pb)^.next;
        PBLock(pb)^.next := NIL;
        pb := p;
        while PBLock(pb)^.next<>NIL do begin
            pb := p^.next;
            dispose(p);
            p := pb;
        end;
        p^.next := p;
        pt := pb;
    end;
end;

procedure CList1.ToBegin;
begin
    pt := pb;
end;

function CList1.IsEnd: boolean;
begin
    IsEnd := ( PBlock(pt)^.next=pb );
end;

procedure CList1.ToNext;
begin
    if IsEnd then SetError(1) else begin
        pt := PBlock(pt)^.next;
        ClearError(254);
    end;
end;

procedure CList1.Insert(x: TElem);
var p: PBlock;
begin
    new(p);
    p^.next := PBlock(pt)^.next;
    p^.data := x;
    PBlock(pt)^.next := p;
    ClearError(253);
end;

procedure CList1.Extract(var x: TElem);
var p: PBlock;
begin
    if IsEnd then SetError(4) else begin
        p := PBlock(pt)^.next;
        x := p^.data;
        PBlock(pt)^.next := p^.next;
        dispose(p);
        ClearError(251);
    end;
end;

procedure CList1.Get(var x: TElem);
begin
    if IsEnd then SetError(64) else begin
        x := PBlock(pt)^.next^.data;
        ClearError(191);
    end;
end;

procedure CList1.Put(x: TElem);
begin
    if IsEnd then SetError(16) else begin
        PBlock(pt)^.next^.data := x;
        ClearError(239);
    end;
end;

procedure CList1.Delete;
var p: PBlock;
begin
    if IsEnd then SetError(32) else begin
        p := PBlock(pt)^.next;
        PBlock(pt)^.next := p;
        dispose(p);
    end;
end;

function CList1.GetError: byte;
begin
    GetError := ErrCode;
end;

procedure CList1.SetError(mask: byte);
begin
    ErrCode := ErrCode or mask;
end;

procedure CList1.ClearError(mask: byte);
begin
    ErrCode := ErrCode and mask;
end;

end.