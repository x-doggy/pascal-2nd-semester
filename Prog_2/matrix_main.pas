program matrix_main;
uses crt, matrix_unit;
var a: PMatrix;
    i, j, dind, uind, lind, rind, key: TInt;
    el, p1, p2, p3, p4: TElem;
    f: file of TElem;
    fname: string;
begin

    a := NIL;

    repeat
        textcolor(10);
        clrscr;

        writeln('      (c) 2014, Владимир Стадник, 4-я прога');
        writeln('    Программа написана в Lubuntu 14.04, amd64');
        writeln(' Программа обработки матриц средствами ООП v. 3.60');
        writeln;
        writeln('1. Создать матрицу');
        writeln('2. Заполнить матрицу вручную');
        writeln('3. Заполнить матрицу псевдослучайно');
        writeln('4. Вывести информацию о матрице');
        writeln('5. Отcортировать матрицу');
        writeln('6. Загрузить матрицу в файл');
        writeln('7. Выгрузить матрицу из файла');
        writeln('9. Выйти из программы');
        write('Введите номер действия: ');
        readln(key);

        case key of
        1:
        begin
            writeln('<Создание матрицы>');
            if (a<>NIL) then begin
                dispose(a, destroy);
                a := NIL;
                writeln('Матрица успешно очищена!');
            end;

            write('Нижний индекс столбца: ');
            readln(lind);
            write('Верхний индекс столбца: ');
            readln(rind);
            write('Нижний индекс строчки: ');
            readln(dind);
            write('Верхний индекс строчки: ');
            readln(uind);

            if (dind<=uind) and (lind<=rind) then begin
                new( a, create(lind, rind, dind, uind) );
                writeln('Матрица создана успешно!');
            end else write('Были указаны неверные индексы');
        end;

        2:
        begin
            writeln('<Заполнение матрицы вручную>');
            for i:=a^.getLowCol to a^.getHighCol do begin
                for j:=a^.getLowRow to a^.getHighRow do begin
                    write('a[', i, ', ', j,'] = ');
                    readln(el);
                    a^.setElem(i, j, el);
                end;
            end;
        end;

        3:
        begin
            writeln('<Заполнение псевдослучайно>');
            randomize;
            if (a=NIL) then writeln('Матрицы не существует')
            else begin
                for i:=a^.getLowRow to a^.getHighRow do begin
                    for j:=a^.getLowCol to a^.getHighCol do begin
                        el := random(501)-250;
                        a^.setElem( j, i, el );
                        write(el:10:2);
                    end;
                    writeln;
                end;
            end;
        end;

        4:
        begin
            writeln('<Вывод информации о массиве>');
            if (a=NIL) then writeln('Матрицы не существует')
            else begin
                for i:=a^.getLowRow to a^.getHighRow do begin
                    for j:=a^.getLowCol to a^.getHighCol do begin
                        a^.getElem( j, i, el );
                        write(el:10:2);
                    end;
                    writeln;
                end;
                writeln('------------------------');
                writeln('нижняя граница индексов столбцов: ', a^.getLowCol );
                writeln('верхняя граница индексов столбцов: ', a^.getHighCol );
                writeln('нижняя граница индексов строк: ', a^.getLowRow );
                writeln('верхняя граница индексов строк: ', a^.getHighRow );
                writeln('------------------------');
                writeln('число столбцов: ', a^.getColCount );
                writeln('число строк: ', a^.getRowCount );
            end;
        end;

        5:
        begin
            writeln('<Сортировка массива>');
            if a<>NIL then a^.shellSort;
        end;

        6:
        begin
            writeln('<Сохранение массива в файл>');
            write('Введите имя файла: ');
            readln(fname);
            assign(f, fname);
            {$I-} rewrite(f); {$I+}
            if IORESULT<>0 then writeln('Ошибка записи в файл')
            else begin
                write(f, a^.getLowCol);
                write(f, a^.getHighCol);
                write(f, a^.getLowRow);
                write(f, a^.getHighRow);
                for i:=a^.getLowRow to a^.getHighRow do begin
                    for j:=a^.getLowCol to a^.getHighCol do begin
                        a^.getElem(j, i, el);
                        write(f, el);
                    end;
                end;
            end;
            close(f);
        end;

        7:
        begin
            writeln('<Выгрузка массива из файла>');
            if (a<>NIL) then begin
                dispose(a, destroy);
                a := NIL;
                writeln('Массив успешно очищен!');
            end;
            write('Введите имя файла: ');
            readln(fname);
            assign(f, fname);
            {$I-} reset(f); {$I+}
            if (IORESULT<>0) or (fname='')
            then writeln('Файла не существует!')
            else begin
                read(f, p1);
                lind := trunc(p1);
                writeln('Нижний индекс для столбца: ', lind);
                read(f, p2);
                rind := trunc(p2);
                writeln('Верхний индекс для столбца: ', rind);
                read(f, p3);
                dind := trunc(p3);
                writeln('Нижний индекс для строчки: ', dind);
                read(f, p4);
                uind := trunc(p4);
                writeln('Верхний индекс для строчки: ', uind);

                if (dind<=uind) and (lind<=rind) then begin
                    new( a, create(lind, rind, dind, uind) );
                    writeln('Основной массив создан успешно!');
                    for i:=a^.getLowRow to a^.getHighRow do begin
                        for j:=a^.getLowCol to a^.getHighCol do begin
                            {$I-} read(f, el); {$I+}
                            if IORESULT<>0
                            then write('Ошибка чтения элемента файла')
                            else a^.setElem(j, i, el);
                        end;
                    end;
                end else write('Неверные индексы');
            end;
            close(f);
        end;
        end;
        if key<>9 then readkey;
    until key=9;
end.
