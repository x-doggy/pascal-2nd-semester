{$mode objfpc}
{$R-}

unit ArrayStack;
(* Реализация стека на основе одномерного дин. массива *)

interface

type
    TUnSignedInt = word;
    TElem = char;
    TArray = array[1..1] of TElem;
    PArray = ^PArray;
    PStackB = ^CStackB;
    CStackB = object
    public
        constructor Init(n: TUnSignedInt);
        destructor Done;
        function IsEmpty: boolean; // проверка на пустоту
        function IsFull: boolean; // проверка на заполненность
        procedure Push(x: TElem); // добавление элемента
        procedure Pop(var x: TElem); // извлечение элемента
    private
        nmax, itop: TUnSignedInt; // nmax-max глубина стека; itop-текущая глубина'
        p: PArray;
        procedure Failure(n: TUnSignedInt); // n - код ошибки
    end;

implementation

const SegSize = 65520;
uses crt;

procedure CStackB.Failure(n: TUnSignedInt);
begin
    clrscr;
    writeln('Ошибка стека N=', n);
    case n of
    1: writeln('Не хватает памяти для стека');
    2: writeln('Добавление в полный стек');
    3: writeln('Выборка из пустого стека');
    end;
end;

constructor CStackB.Init;
var need: longint;
begin
    nmax := n;
    need := longint(n) * sizeof(TElem);
    if (need>SegSize) or (need>maxavail) then Failure(1);
    getmem(p, k);
    itop := 0;
end;

destructor CStackB.Done;
begin
    freemem(p, nmax * sizeof(TElem));
    nmax := 0;
    itop := 0;
    p := NIL;
end;

function CStackB.IsEmpty: boolean;
begin
    IsEmpty := (itop=0);
end;

function CStackB.IsFull: boolean;
begin
    IsFull := (itop=nmax);
end;

procedure CStackB.Push(x: TElem);
begin
    if itop=nmax then Failure(2);
    inc(itop);
    p^[itop] := x;
end;

procedure CStackB.Pop(var x: TElem);
begin
    if itop=0 then Failure(3);
    x := p^[itop];
    dec(itop);
end;

begin
    {$R+}
end.