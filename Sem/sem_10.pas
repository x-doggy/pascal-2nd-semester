unit s2_sem_1;

interface

const MAX = 250;
type
    TElem        = single;
    TRecord      = record
                       key: single;
                       x:   TElem;
                   end;
    TArray       = array[1..MAX] of TRecord;
    TUnSignedInt = word;

procedure binsor( var a: TArray; n: TUnSignedInt );
    
implementation

procedure binsor( var a: TArray; n: TUnSignedInt );
var i, j, l, r, m: TUnSignedInt; x: TRecord;
begin
    for i:=2 to n do begin
        x := a[i];
        l := 1;
        r := i-1;
        while l<=r do begin
            m := (l+r) div 2;
            if x.key < a[m].key
            then r := m-1
            else l := m+1;
        end;
        for j:=i-1 downto 1 do a[j+1] := a[j];
        a[l] := x;
    end;
end.