{$mode objfpc}
{$R-}

unit seminar_11;

(*
 * 
 * Copyright (c) 2014 владимир Стадник, <x-doggy@ya.ru>
 * Данный исходный код на 95% писал я и распространяю по лицензии GNU GPL
 * Это означает, что вы можете:
 *** Свободно изучать исходный код;
 *** Без обязательного согласия автора свободно передавать код другому
 *** Свободно модифицировать код, сохраняя имя автора изначального варианта


Тема: Реализация АТД "ограниченная очередь" (и "ограниченный дек")
на основе одномерного динамического массива.

Поля: (не)типизированный указатель, индекс левого конца, индекс буфера
(на 1 больше индекса правого конца),
максимальное число элементов очереди (дека),
код ошибки (число типа TShortUnSignedInt).
Методы:
1. Создать очередь (дек) максимальной длины n.
2. Скопировать очередь (дек) с готовой (конструктор копирования).
3. Уничтожить очередь (дек).
4. Сделать пустой (пустым).
5. Очередь пуста (дек пуст)?
6. Очередь полна (дек полон)?
7. Добавить элемент (для дека – справа и слева).
8. Взять элемент (для дека – справа и слева).
9. Посмотреть элемент (для дека – справа и слева).
10. Изменить элемент (для дека – справа и слева).
11. Удалить элемент (для дека – справа и слева).
12. Вернуть код ошибки (с одновременной очисткой).
Каждый метод класса имеет свой код ошибки, устанавливает его.
если ошибка произошла, и очищает, если метод отработал нормально.
Требуется написать модуль, в части interface которого описание класса
и типа-указателя на класс, а в части implementation -
реализация всех методов класса.
*)

interface

type
    TShortInt = byte;
    TInt      = integer;
    TElem     = single;
    PData     = ^TData;
    TData     = TElem;
    TArr      = array[1..1] of TElem;
    PArr      = ^TArr;
    PQueue    = ^CQueue;
    CQueue    = object
    private
        data: PArr;
        maxlen, head, tail: TInt;
        ce: TShortInt;
    public
        constructor create(n: TInt);
        constructor copy(d: PQueue);
        destructor  destroy;
        procedure makeEmpty;  // сделать пустым
        function error: TShortInt; // код ошибки
        function isEmpty: boolean; // пустой?
        function isFull:  boolean; // полный?
        function getLength: TInt; // максимальная длина возвращается
        procedure popHead(var d: TData); // взять голову
        procedure getHead(var d: TData); // посмотреть голову
        procedure changeHead(var d: TData);
        procedure pushTail(d: TData); // добавить в хвост
        procedure delHead; // удалить голову
        procedure Invert;  // инвертирование очереди с помощью стека
        //function getTail(var d: TData);
        //function popTail(var d: TData);
        //procedure pushHead;
        //procedure removeTail;
        //procedure changeTail(d: TArr);
    end;

implementation

constructor CQueue.create(n: TInt);
var need: longint;
begin
    if (n>0) then begin
        ce := 0;
        maxlen := n;
        need := sizeof(TData) * n;
        getmem( data, need );
        head := 0;
        tail := 0;
    end else ce := 1;
end;

constructor CQueue.copy(d: PQueue);
var need: longint; i: TInt;
begin
    if d = nil then begin
        ce := 0;
        maxlen := d^.maxlen;
        tail := d^.tail;
        head := d^.head;
        need := maxlen * sizeof(TData);
        getmem( data, need );
        for i:=head to tail do
            data^[i] := d^.data^[i];
    end else ce := 2;
end;

destructor CQueue.destroy;
var need: longint;
begin
    if (maxlen > 0) then begin
        need := sizeof(TData) * maxlen;
        freemem( data, need );
        ce := 0;
    end else ce := 3;
end;

procedure CQueue.makeEmpty;
begin
    tail := 0;
    head := 0;
end;

function CQueue.isEmpty: boolean;
begin
    isEmpty := (head = tail);
end;

function CQueue.isFull: boolean;
begin
    isFull := (head = (tail + 1) mod maxlen);
end;

procedure CQueue.getHead(var d: TData);
begin
    if not isEmpty then begin
        d := data^[head];
        ce := 0;
    end else ce := 4;
end;

procedure CQueue.pushTail(d: TData);
var x: TInt;
begin
  if not isFull then begin
      x := tail mod (maxlen + 1);
      data^[x] := d;
      tail := (tail + 1) mod maxlen;
      ce := 0;
  end else ce := 5;
end;

procedure CQueue.popHead(var d: TData);
begin
    if head <> (tail + 1) mod maxlen then begin
        d := data^[head];
        head := (head+1) mod maxlen;
        ce := 0;
    end else ce := 6;
end;

procedure CQueue.delHead;
begin
    if not isEmpty then begin
        head := (head - 1) mod maxlen;
        ce := 0;
    end else ce := 7;
end;

procedure changeHead(var d: TData);
begin
    if not isEmpty then begin
        data^[head] := d;
    end else ce := 9;
end;

procedure CQueue.Invert;
var s: PStackB; elem: TElem;
begin
    if isEmpty then ce := 8 else begin
        ce := 0;
        new(s, Init);
        while not isEmpty do begin
            popHead(elem);
            s^.Push(elem);
        end;
        while not s^.isEmpty do begin
            s^.Pop(elem);
            pushTail(elem);
        end;
        dispose(s, Done);
    end;
end;

function CQueue.getLength: TInt;
begin
    getLength := maxlen;
end;

function CQueue.error: TShortInt;
begin
    error := ce;
    ce := 0;
end;

end.
