{$mode objfpc}
{$R-}

(* Написать подпрограмму сортировки строк матрицы (объекта класса, реализованного
на предыдущем занятии) шейкер-сортировкой по невозрастанию максимальной длины
цепочки идущих положительных элементов. Перед сортировкой создать массив ключей,
длина которого равна количеству строк, а элементы – максимальные длины указанных
цепочек в соответствующих строках. Подпрограмма должна быть реализована как
дружественная функция класса «матрица» (т. е. В том же модуле, где сам класс). *)

unit seminar_10;

interface

uses seminar_11, stack;
function ShakeSortF(p: PMatrix): boolean;
function Matrix.ShakeSortC: boolean;

implementation

function ShakeSortF(p: PMatrix): boolean;
type TIntArr = array[1..1] of TUnSignedInt;
     PIntArr = ^TIntArr;
var n, m, t, l, r: TUnSignedInt;
    x:             TElem;
    q1, q2:        TLongSignedInt;
    buf:           PArr;
    a:             TIntArr;
begin
    if p=NIL then ShakeSort := false else begin
        ShakeSort := true;
        n := p^.GetRowCount();
        m := p^.GetColCount();
        getmem( a, longint(n) * sizeof(TUnSignedInt) );

        // формирование массива ключей
        for i:=1 to n do begin
            q1 := 0;
            q2 := 0;
            for j:=1 to n do begin
                x := p^.matr^[i]^[j];
                if x>0 then inc(q1) else q1 := 0;
                if q2<q1 then q2 := q1;
            end;
            a^[i] := q2;
        end;

        t := 2;
        b := n;
        i := 2;

        while (i<=n) and (t<>0) and (b<>n+1) do begin
            r := n-1;
            for j:=b downto 1 do 
                if a^[j-1] > a^[j] then begin
                    buf := p^.matr^[j-1];
                    p^.matr^[j-1] := p^.matr^[j];
                    p^.matr^[j] := buf;
                    (* Для недружественной ф-ии см. *** *)
                    q1 := a^[j-1];
                    a^[j-1] := a^[j];
                    a^[j] := q1;
                    k := j;
                end;
            t := k;
            if t=n+1 then break;
            k := 0;
            for j:=t-1 to b do
                if a^[j-1] > a^[j] then begin
                    buf := p^.matr^[j-1];
                    p^.matr^[j-1] := p^.matr^[j];
                    p^.matr^[j] := buf;
                    q1 := a^[j-1];
                    a^[j-1] := a^[j];
                    a^[j] := q1;
                    k := j-1;
                end;
        end;
        freemem( a, longint(n) * sizeof(TUnSignedInt) );
    end;
end;

(*
 *** Сортировка ук-лей для недр. ф-ции

p^.getPointer(buf, j-1);
p^.setPointer(buf, j);
p^.getPointer(j, buf);
p^.setPointer(j-1, buf);


procedure Matrix.getPointer(var point: PArr; nrow: TInt);
begin
    if (nrow > high1) or (nrow < low1) then
        fatalerror(7)
    else
        point := massive^[nrow];
end;

procedure Matrix.setPointer(nrow: TInt; point: PArr);
begin
    if (nrow > high1) or (nrow < low1) then
        fatalerror(8)
    else
        massive^[nrow] := point;
end;

*)