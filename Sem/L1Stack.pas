{$mode objfpc}
{$R-}

unit L1Stack;
(*Реализация стека на основе односвязного списка*)

interface

type TUnSignedInt = integer;
     TShortUnSignedInt = byte;
     TElem = TUnSignedInt;
     PStack = ^CStack; // существующий стек
     CStack = object
        private
            p: pointer; // используется нетипизированный указатель
            ErrCode: TShortUnSignedInt;
            function GetError: TShortUnSignedInt;
            // чтобы скрыть реализацию от пользователя
        public
            constructor Init; // создаёт пустой стек
            //constructor Copy(Ps: PStack); // копирование с другого
            destructor  Done; // уничтожение
            procedure Push(x: TElem); // добавить элемент
            procedure Pop(var x: Telem); // извлечь элемент
            function IsEmpty: boolean; // проверка на пустоту
            procedure SeeTop(var x: TElem); // посмотреть вершину
            procedure ChangeTop(x: TElem); // изменить вершину
            procedure MakeEmpty; // сделать пустым
            procedure DelTop; // удалить вершину
    end;

implementation

type PBlock = ^TBlock;
     TBlock = record
        data: TElem;
        next: PBlock;
     end;
const BSize = sizeof(TBlock);

constructor CStack.Init;
begin
    p := NIL;
    ErrCode := 0;
end;

destructor CStack.Done;
begin
    MakeEmpty;
end;

procedure CStack.Push(x: TElem);
var p1: PBlock;
begin
        new(p1);
        p1^.next := p;
        p1^.data := x;
        p := p1;
        ErrCode := 0;
end;

procedure CStack.Pop(var x: TElem);
var p1: PBlock;
begin
    if p=NIL then ErrCode := 2 else begin
        p1 := PBlock(p);
        p := p1^.next;
        x := p1^.data;
        dispose(p1);
        ErrCode := 0;
    end;
end;

function CStack.IsEmpty: boolean;
begin
    IsEmpty := (p = NIL);
end;

procedure CStack.SeeTop(var x: TElem);
begin
    if p=NIL then ErrCode := 3 else begin
        x := PBlock(p)^.data;
        ErrCode := 0;
    end;
end;

procedure CStack.ChangeTop(x: TElem);
begin
    if p=NIL then ErrCode := 4 else begin
        PBlock(p)^.data := x;
        ErrCode := 0;
    end;
end;

procedure CStack.DelTop;
var p1: PBlock;
begin
    if p=NIL then ErrCode := 5 else begin
        p1 := PBlock(p);
        p := p1^.next;
        dispose(p1);
        ErrCode := 0;
    end;
end;

procedure CStack.MakeEmpty;
var p1: PBlock;
begin
    while p<>NIL do begin
        p1 := PBlock(p);
        p := p1^.next;
        dispose(p1);
    end;
    ErrCode := 0;
end;

function CStack.GetError: TShortUnSignedInt;
begin
    GetError := ErrCode;
    ErrCode := 0;
end;

begin
    {$R+}
end.
