program main8;
uses crt, task8_unit;
type TDiap = 0..10;
var
    ch:               TDiap;
    e, elem:          TElem;
    f, g:             TFile;
    fname:            string;
    am, poslen, amount, f1, f2, i: integer;
    r:                single;

begin {main}
    textcolor(yellow);

    repeat
        clrscr;
        writeln('        Программа обработки последовательности');
        writeln('        (c) 2014, Владимир Стадник, 1-я прога');
        writeln('    Программа была написана на Kubuntu 13.10 amd64');
        writeln;
        writeln('    №8  Дана последовательность символов, заданная в');
        writeln('   типизированном файле. Методом индуктивной функции');
        writeln('    определить среднюю длину подпоследовательностей,');
        writeln('                состоящих из четных цифр.');
        writeln;
        writeln('1. Сгенерировать последовательность');
        writeln('2. Обработать полученную последовательность');
        writeln('3. Ввести последовательность с клавиатуры');
        writeln('4. Продвинутый тип взаимодействия');
        writeln('...');
        writeln('9. Выйти из программы');
        writeln;
        write('Введите номер действия:  ');
        readln(ch);

        case ch of
            1:
            begin
                write('Введите кол-во элементов в посл-сти: ');
                readln(am);
                write('Введите длину последовательности: ');
                readln(poslen);
                write('Введите кол-во подпоследовательностей в файле: ');
                readln(amount);
                write('Введите имя файла для записи на диск: ');
                readln(fname);
                f1 := generateSequence(g, fname, am, poslen, amount);
                case f1 of
                    -1: writeln('1: Ошибка перезаписи/создания файла на диске');
                    -2: writeln('2: Ошибка перевода каретки в начало файла');
                    -3: writeln('3: Ошибка закрытия файла');
                    else writeln('Файл ', fname, ' был успешно создан.');
                end; {case, f1}
            end; {case, 1}
            2:
            begin
                write('Введите имя файла для чтения из диска: ');
                readln(fname);
                assign(f, fname);
                reset(f);
                f2 := featureSequence(f, r);
                case f2 of
                    -1: writeln('1: Ошибка чтения файла на диске.');
                    -2: writeln('2: Ошибка перевода каретки в начало файла.');
                    -3: writeln('3: Ошибка чтения значения из файла.');
                    -4: writeln('4: Ошибка закрытия файла.');
                    else begin
                        writeln('Файл ', fname, ' был успешно обработан');
                        assign(f, fname);
                        {$I-} reset(f); {$I+}
                        if ioresult=0 then begin
                            repeat
                                {$I-} read(f, elem); {$I+}
                                write(elem, '   ');
                            until (eof(f)) and (ioresult=0);
                            writeln;
                            writeln('Результат - средняя длина четных подпосл-стей: ', r:8:2);
                        end;
                        {$I-} close(f); {$I+}
                        if ioresult<>0 then writeln('Произошла ошибка закрытия файла');
                     end; {case, f2, 1}
                end; {case, f2}
            end; {case, 2}
            3:
            begin
                write('Введите кол-во элементов в посл-сти: ');
                readln(am);
                write('Введите имя файла для записи на диск: ');
                readln(fname);

                assign(f, fname);
                {$I-} rewrite(f); {$I+}
                if ioresult<>0 then f1 := -1;
                {$I-} seek(f, 0); {$I+}
                if ioresult<>0 then f1 := -2;

                for i:=1 to am do begin
                    write('Введите, пожалуйста, ', i, 'ый элемент: ');
                    readln(e);
                    {$I-} write(f, e); {$I+}
                    if ioresult<>0 then f1 := -1 else writeln('OK!');
                end;
            end;
            4:
            begin
                
            end;
        end; {case, ch}
        if ch<>9 then readkey;
    until ch=9;
    writeln('Спасибо за пользование программой :-)');
end.
