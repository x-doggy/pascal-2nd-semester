unit task8_unit;

interface

type
    TElem  = char;
    TFile  = file of TElem;

function generateSequence( var f: TFile; maskname: string; var howmuch, kolpos, lenpos: integer): integer;
function featureSequence( var f: TFile;  var res: single ): integer;
function CharToInt(var chrsym: char): integer;

implementation

function CharToInt(var chrsym: char): integer;
begin
    if (chrsym <= '0') or (chrsym >= '9') then exit else begin
        CharToInt := ord(chrsym) - 48;
    end;
end;

function generateSequence( var f: TFile; maskname: string; var howmuch, kolpos, lenpos: integer): integer;
{
Функция создает типизированный файл на диске, содержащий в себе последовательность
случайно распределенных целых чисел.
На вход подпрограмма принимает файловую переменную f для связи с файлом на внешнем носителе,
имя файла maskname с той же целью, параметр howmuch задает кол-во чисел в последовательности:
если получено отрицательное значение, то оно переводится в положительное, ежели получен ноль,
то программа по умолчанию устанавливает значение 5.
lenpos задаёт ...
kolpos задаёт кол-во последовательностей в файле
Через основной поток возвращаемого значения функция передаёт код ошибки.
- Коды ошибок:
   -1       Ошибка перезаписи/создания файла на диске.
   -2       Ошибка перевода каретки в начало файла.
   -3       Ошибка закрытия файла.
}
var i, a, rndkol, rndlen, kolfree, lenex, kolex, freelenex: integer;
begin
    if howmuch < 0 then begin
        if howmuch=0 then howmuch := 5;
        howmuch := -howmuch;
    end;
    assign(f, maskname);
    {$I-} rewrite(f); {$I+}
    if ioresult<>0 then generateSequence := -1 else begin
        {$I-} seek(f, 0); {$I+}
        if ioresult<>0 then generateSequence := -2 else begin
            kolfree    := lenpos-kolpos;
            kolex      := kolpos;
            lenex      := howmuch-lenpos;
            freelenex  := lenex-kolex;
            if howmuch <= kolpos then generateSequence := -4 else begin

                // псевдослучайный выбор четной/нечетной цифры
                a := random(2);
                repeat
                    if a=0 then begin // обработка четной цифры
                        if kolpos > 0 then begin
                            repeat
                                rndlen:=random(10) + 48; // генерация цифры
                            until rndlen mod 2=0;
                            {$I-} write(f, chr(rndlen)); {$I+}
                            if ioresult<>0 then generateSequence := -3
                            else begin
                                kolpos := kolpos - 1;
                                howmuch := howmuch - 1;
                            end;
                        end;
                        if kolfree > 0 then begin
                            rndkol:=random(kolfree+1);
                            if rndkol<>0 then
                                for i:=1 to rndkol do begin
                                    repeat
                                        rndlen:=random(10) + 48;
                                    until rndlen mod 2=0;
                                    {$I-} write(f, chr(rndlen)); {$I+}
                                    if ioresult<>0 then generateSequence := -1
                                    else begin
                                        howmuch:=howmuch-1;
                                        kolfree:=kolfree-1;
                                    end;
                                end;
                        end;
                        if (kolpos=0) and (kolfree>0) then begin
                            for i:=1 to kolfree do begin
                                repeat
                                    rndlen:=random(10) + 48;
                                until rndlen mod 2=0;
                                {$I-} write(f, chr(rndlen)); {$I+}
                                if ioresult<>0 then generateSequence := -1
                                else begin
                                    howmuch:=howmuch-1;
                                    kolfree:=kolfree-1;
                                end;
                            end;
                        end;
                        a:=1;
                    end;
                    if a=1 then begin // нечётная цифра
                        if kolex > 0 then begin
                            repeat
                                rndlen := random(254) + 1; // генерация
                            until (rndlen<>48) and (rndlen<>50) and (rndlen<>52) and (rndlen<>54) and (rndlen<>58);
                            {$I-} write(f, chr(rndlen)); {$I+}
                            if ioresult<>0 then generateSequence := -1
                            else begin
                                kolex := kolex-1;
                                howmuch := howmuch-1;
                            end;
                        end;
                        if freelenex > 0 then begin
                            rndkol := random(freelenex+1);
                            if rndkol<>0 then
                                for i:=1 to rndkol do begin
                                    repeat
                                        rndlen := random(254) + 1;
                                    until (rndlen<>48) and (rndlen<>50) and (rndlen<>52) and (rndlen<>54) and (rndlen<>58);
                                    {$I-} write(f, chr(rndlen)); {$I+}
                                    if ioresult<>0 then generateSequence := -1
                                    else begin
                                        howmuch := howmuch-1;
                                        freelenex := freelenex-1;
                                    end;
                                end;
                        end;
                        if (kolex=0) and (freelenex>0) then begin
                            for i:=1 to freelenex do begin
                                repeat
                                    rndlen := random(254) + 1;
                                until (rndlen<>48) and (rndlen<>50) and (rndlen<>52) and (rndlen<>54) and (rndlen<>58);
                                {$I-} write(f, chr(rndlen)); {$I+}
                                if ioresult<>0 then generateSequence := -1
                                else begin
                                    howmuch := howmuch-1;
                                    freelenex := freelenex-1;
                                end;
                            end;
                        end;
                        a:=0;
                    end;
                until howmuch=0;
            end;
        end;
    end;
    {$I-} close(f); {$I+}
    if ioresult<>0 then generateSequence := -3 else generateSequence := 1;
end;

function featureSequence( var f: TFile; var res: single ): integer;
{
Функция определяет среднюю длину посл-стей, состоящих из чётных цифр.
Входной поток таков: f - файловая переменная для файла, содержащего посл-сть,
filename - имя файла на внешнем носителе для f.
Выходной поток состоит из параметра-переменной res, в котором будет содержаться
значение средней длины, либо -1, если посл-сть не имеет какую-либу четную подпосл-сть,
и из основного потока возвращаемого значения ф-ции, в котором хранится код ошибки.
- Коды ошибок:
   -1       Ошибка чтения файла на диске.
   -2       Ошибка перевода каретки в начало файла.
   -3       Ошибка чтения значения из файла.
   -4       Ошибка закрытия файла.
}
var len, kol: integer; c, h: TElem; flag: boolean; fpos: longint;
begin
    if ioresult<>0 then featureSequence := -1 else begin
        fpos := filepos(f);
        featureSequence := 1;
        {$I-} seek(f, 0); {$I+}
        if ioresult<>0 then featureSequence := -2 else begin
            len := 0;
            kol := 0;
            repeat
                {$I-} read(f, c); {$I+}
                if (ioresult<>0) then featureSequence := -3
                else begin
                    flag := false;
                    if c in ['0','2','4','6','8'] then begin
                        kol := kol + 1;
                        len := len + 1;
                        while (not eof(f)) and (c in ['0','2','4','6','8']) do begin
                            {$I-} read(f, c); {$I+}
                            if (ioresult<>0) and (c in ['0','2','4','6','8']) then featureSequence := -3;
                            if c in ['0','2','4','6','8'] then len := len + 1;
                        end;
                    end;
                end;
            until eof(f);
            flag := true;
            if kol=0 then res := -1.0 else begin
                res := len / kol;
                {$I-} seek(f, fpos); {$I+}
                if ioresult<>0 then featureSequence := -2;
            end;
        end;
    end;
end;

end.
