program Integral_test;
uses crt;
const REC_PARAM = 64;
      ITERS    = 150;
type TUnSignedInt  = word;
     TReal         = double;
     TFunc         = function(x: TReal): TReal;
     TFInt         = function(a, b: TReal): TReal;
var Integ, Mark, eps, low, high: TReal;
    key, seg, err: TUnSignedInt;
    isExit: boolean;
    fun: TFunc;
    fint: TFInt;

function Integral(a, b: TReal; f: TFunc; p: TUnSignedInt; eps: TReal): TReal;
{
* a, b     - нижняя и верхняя границы интегрирования;
* f        - имя подпрограммы для подынтегральной функции;
* p        - число разбиений (шаг сетки);
* eps>0    - критерий для оценки Рунге;
* Подпрограмма возвращает приближенное значение интеграла.
* Применялась формула 3 на стр. 14
}
var h, sum: TReal; i: TUnSignedInt;
begin
    if (p>0) and (a<>b) and (f<>NIL) then begin
        h := (b-a) / p;
        sum := 0;
        for i:=1 to p do sum := sum + f(a+h*i-h/2);
        sum := sum * h;
        Integral := sum;
    end;
end;

function MarkInteg(a, b: TReal; fd: TFunc; p: TUnSignedInt; epsilon: TReal; var ce: word): TReal;
{
Оценка погрешности интеграла, ф-ла стр. 19
* a, b     - верхний/нижний пределы интегрирования;
* fd       - функция, которая интегрируется;
* p        - число разбиений отрезка интегрирования;
* eps>0    - критерий для оценки Рунге;
Функция вернёт 0, если количество итераций превысело максимально возможное
(т.е. не была достигнута необходимая точность), иначе - полученную погрешность.
}
var int1, int2: TReal; itr: byte;
begin
    if (p>0) and (fd<>NIL) then begin
        itr := 0;
        repeat
            int1 := Integral(a, b, fd, p, epsilon);
            int2 := Integral(a, b, fd, 2*p, epsilon);
            inc(itr);
        until (abs(int2-int1) <= epsilon) or (itr>=ITERS);
        if itr=ITERS then err := 1 else err := 0;
        MarkInteg := (int2-int1)/3;
    end;
end;

function f11_1(x: TReal): TReal;
{Функция № 11 п. 1}
begin
    f11_1 := exp(-x * exp( (1/3) * ln( sqr(x) - 1 ) ));
end;

function f11_2(x: TReal): TReal;
{Функция № 11 п. 2}
begin
    f11_2 := exp(ln(3) * sin(x));
end;

function ex(x: TReal): TReal;
{Функция-экспонента}
begin
    ex := exp(x);
end;

function sinx(x: TReal): TReal;
{Функция-синус}
begin
    sinx := sin(x);
end;

function int_sin(a, b: TReal): TReal;
{Первообразная синуса}
begin
    int_sin := cos(a) - cos(b);
end;

function int_exp(a, b: TReal): TReal;
{Первообразная экспоненты}
begin
    int_exp := exp(b) - exp(a);
end;

begin {main}
    textcolor(15);
    isExit := false;
    fint := NIL;
    Integ := 0;
    Mark := 0;

    repeat
        clrscr;
        writeln('    (c) 2014, Владимир Стадник, 4-я прога');
        writeln('   Программа написана в Kubuntu 14.04, amd64');
        writeln('Численное интегрирование квадратурными формулами');
        writeln('    Формула прямоугольников, интегралы № 11');
        writeln;
        textcolor(11);
        writeln('1. exp(-x * exp( (1/3) * ln( sqr(x) - 1 ) ))');
        writeln('2. exp(ln(3) * sin(x))');
        writeln('3. e^x');
        writeln('4. sin x');
        textcolor(15);
        writeln('5. Выйти в операционную среду');
        
        write('Введите № функции: ');
        repeat
            {$I-} readln(key); {$I+}
        until (IORESULT=0) and (key<6);
        
        if key<>5 then begin
            write('Введите погрешность: ');
            repeat
                {$I-} readln(eps); {$I+}
            until IORESULT=0;
        end;
        
        case key of 
            1:
            begin
                low   := 1;
                high  := 5;
                fun   := @f11_1;
                fint  := NIL;
                seg   := REC_PARAM;
            end;
            
            2:
            begin
                low   := 1;
                high  := 3;
                fun   := @f11_2;
                fint  := NIL;
                seg   := REC_PARAM;
            end;
            
            3:
            begin
                write('Введите пределы интегрирования [a, b]: ');
                repeat
                    {$I-} readln( low, high ); {$I+}
                until IORESULT=0;
                
                write('Введите число разбиений: ');
                repeat
                    {$I-} readln( seg ); {$I+}
                until (IORESULT=0) and (seg > 0);

                fun := @ex;
                fint := @int_exp;
            end;
            
            4:
            begin
                write('Введите пределы интегрирования [a, b]: ');
                repeat
                    {$I-} readln( low, high ); {$I+}
                until IORESULT=0;
                
                write('Введите число разбиений: ');
                repeat
                    {$I-} readln( seg ); {$I+}
                until (IORESULT=0) and (seg > 0);

                fun := @sinx;
                fint := @int_sin;
            end;
            
            5: isExit := true;
        end;
        
        if key<>5 then begin
            writeln;
            if fint<>NIL then writeln('Интеграл по формуле Ньютона-Лейбница = ', fint(low, high));
            Integ := Integral(low, high, fun, seg, eps);
            writeln('Интеграл по квадратурной формуле = ', Integ);
            Mark := MarkInteg(low, high, fun, seg, eps, err);
            if err=0 then writeln('Оценка погрешности интеграла = ', Mark)
            else writeln('Интеграл расходящийся');
        end;

        if key<>5 then readkey;
    until isExit;
end.
