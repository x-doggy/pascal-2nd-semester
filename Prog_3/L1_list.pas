{$mode objfpc}
{$R-}{$Q+}

unit L1_list;

interface

type TElem = integer;
     PList1 = ^CList1;
     CList1 = object
         private
            pb, pt: pointer; // pb-указатель на буфер, pt-указатель на текущий элемент списка
            ErrCode: byte;
            procedure SetError(mask: byte);
            procedure ClearError(mask: byte);
         public
            constructor Init;                // создать L1-список
            constructor Copy(pl1: Plist1);   // скопировать в готовой
            destructor Done;                 // разрушить L1-список
            function GetError: byte;         // вернуть код ошибки
            function IsEmpty: boolean;       // L1-список пуст?
            function IsEnd: boolean;         // указатель в конце L1-списка?
            procedure MakeEmpty;             // сделать пустым
            procedure ToBegin;               // перенести указатель в начало
            procedure ToNext;                // переместить указатель дальше
            procedure Insert(x: TElem);      // добавить элемент
            procedure Extract(var x: TElem); // удалить элемент и вернуть
            procedure Get(var x: TElem);     // взять элемент
            procedure Put(x: TElem);         // изменить элемент
            procedure Delete;                // удалить элемент
     end;

implementation

type PBlock = ^TBlock;
     TBlock = record
         data: TElem;
         next: PBlock;
     end;

constructor CList1.Init;
var p: PBlock;
begin
    new(p);
    p^.next := p;
    pb := p;
    pt := p;
end;

destructor CList1.Done;
begin
    MakeEmpty;
    dispose(PBlock(pb));
end;

constructor CList1.Copy(pl1: PList1);
var p: PBlock;
    x: TElem;
begin
    if pl1=NIL then fail else begin
        new(p);
        p^.next := p;
        pb := p;
        pt := p;
        pl1^.ToBegin;
        while not pl1^.IsEnd do begin
              pl1^.Get(x);
              Insert(x);
              pl1^.ToNext;
              ToNext;
        end;
    end;
end;

function CList1.IsEmpty: boolean;
begin
    IsEmpty := ( PBLock(pb)^.next = pb );
end;

procedure CList1.ToBegin;
begin
    pt := pb;
end;

function CList1.IsEnd: boolean;
begin
    IsEnd := ( PBlock(pt)^.next = pb );
end;

procedure CList1.ToNext;
begin
    if IsEnd then SetError(1) else begin
        pt := PBlock(pt)^.next;
        ClearError(254);
    end;
end;

procedure CList1.Insert(x: TElem);
var p: PBlock;
begin
    new(p);
    p^.next := PBlock(pt)^.next;
    p^.data := x;
    PBlock(pt)^.next := p;
    ClearError(253);
end;

procedure CList1.Extract(var x: TElem);
var p: PBlock;
begin
    if IsEnd then SetError(4) else begin
        p := PBlock(pt)^.next;
        x := p^.data;
        PBlock(pt)^.next := p^.next;
        dispose(p);
        ClearError(251);
    end;
end;

procedure CList1.Get(var x: TElem);
begin
    if IsEnd then SetError(64) else begin
        x := PBlock(pt)^.next^.data;
        ClearError(191);
    end;
end;

procedure CList1.Put(x: TElem);
begin
    if IsEnd then SetError(16) else begin
        PBlock(pt)^.next^.data := x;
        ClearError(239);
    end;
end;

procedure CList1.Delete;
var p: PBlock;
begin
    if IsEnd then SetError(32) else begin
        p := PBlock(pt)^.next;
        PBlock(pt)^.next := p^.next;
        dispose(p);
    end;
end;

procedure CList1.MakeEmpty;
//var p: PBlock;
begin
    if PBlock(pb)^.next<>pb then begin
        {p := PBlock(pb)^.next;
        PBLock(pb)^.next := NIL;
        pb := p;}
        while {PBLock(pb)^.next<>NIL} not IsEnd do begin
            {pb := p^.next;
            dispose(p);
            p := pb;}
            Delete;
        end;
        {p^.next := p;
        pt := pb;}
    end;
end;

function CList1.GetError: byte;
begin
    GetError := ErrCode;
end;

procedure CList1.SetError(mask: byte);
begin
    ErrCode := ErrCode or mask;
end;

procedure CList1.ClearError(mask: byte);
begin
    ErrCode := ErrCode and mask;
end;

begin {initialization}

end.
