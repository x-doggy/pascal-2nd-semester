{$mode objfpc}
{$R-}{$Q+}

unit L1Queue_unit;

interface

uses crt, L1_list;
type TElem = integer;
     TShortUnSignedInt = byte;
     PL1Queue = ^CL1Queue;
     CL1Queue = object
         private
            q: PList1;
            ErrCode: byte;
            function getError: TShortUnSignedInt; // получение кода ошибки
         public
            constructor Init; // создаёт пустой стек
            constructor Copy(Pq: PL1Queue); // копирование с другого
            destructor  Done; // уничтожение
            function IsEnd: boolean;
            function IsEmpty: boolean; // проверка на пустоту
            procedure MakeEmpty; // сделать пустым
            procedure PushTail(x: TElem); // добавить элемент
            procedure PopHead(var x: Telem); // извлечь элемент
            procedure GetHead(var x: TElem); // посмотреть вершину
            procedure ChangeHead(x: TElem); // изменить вершину
            procedure DelHead; // удалить вершину
     end;

implementation

constructor CL1Queue.Init;
begin
    new(q, Init);
end;

constructor CL1Queue.Copy(Pq: PL1Queue);
begin
    new(q, Copy(Pq^.q));
end;

destructor CL1Queue.Done;
begin
    if not isEmpty then begin
        dispose(PList1(q), Done);
    end else ErrCode := 2;
end;

procedure CL1Queue.MakeEmpty;
begin
    if not IsEmpty then q^.MakeEmpty else ErrCode := 3;
end;

function CL1Queue.IsEmpty: boolean;
begin
    IsEmpty := (q^.IsEmpty);
end;

function CL1Queue.IsEnd: boolean;
begin
    IsEnd := (q^.IsEnd);
end;

procedure CL1Queue.PushTail(x: TElem);
begin
    while not IsEnd do q^.ToNext;
    q^.Insert(x);
end;

procedure CL1Queue.PopHead(var x: TElem);
begin
    if not IsEmpty then begin
        q^.ToBegin;
        q^.Extract(x);
    end else ErrCode := 4;
end;

procedure CL1Queue.GetHead(var x: TElem);
begin
    if not IsEmpty then begin
        q^.ToBegin;
        q^.Get(x);
    end else ErrCode := 5;
end;

procedure CL1Queue.ChangeHead(x: TElem);
begin
    if not IsEmpty then begin
        q^.ToBegin;
        q^.Put(x);
    end else ErrCode := 6;
end;

procedure CL1Queue.DelHead;
begin
    if not IsEmpty then begin
        q^.ToBegin;
        q^.Delete;
    end else ErrCode := 7;
end;

function CL1Queue.getError: TShortUnSignedInt;
begin
    getError := q^.GetError or ErrCode;
    halt(4);
end;

begin {initialization}

end.
