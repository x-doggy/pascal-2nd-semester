{$mode objfpc}
{$R-}{$Q+}

unit L1Stack_unit;

interface

uses crt, L1_list;
type TElem = integer;
     TShortUnSignedInt = byte;
     PL1Stack = ^CL1Stack;
     CL1Stack = object
         private
            s: PList1;
            ErrCode: byte;
            function getError: TShortUnSignedInt; // получение кода ошибки
         public
            constructor Init; // создаёт пустой стек
            constructor Copy(Ps: PL1Stack); // копирование с другого
            destructor  Done; // уничтожение
            function IsEmpty: boolean; // проверка на пустоту
            procedure MakeEmpty; // сделать пустым
            procedure Push(x: TElem); // добавить элемент
            procedure Pop(var x: Telem); // извлечь элемент
            procedure SeeTop(var x: TElem); // посмотреть вершину
            procedure ChangeTop(x: TElem); // изменить вершину
            procedure DelTop; // удалить вершину
     end;

implementation

constructor CL1Stack.Init;
begin
    new(s, Init);
end;

constructor CL1Stack.Copy(Ps: PL1Stack);
begin
    new(s, Copy(Ps^.s))
end;

destructor CL1Stack.Done;
begin
    dispose(PList1(s), Done);
end;

function CL1Stack.IsEmpty: boolean;
begin
    IsEmpty := (s^.IsEmpty);
end;

procedure CL1Stack.MakeEmpty;
begin
    if not IsEmpty then s^.MakeEmpty else ErrCode := 3;
end;

procedure CL1Stack.Push(x: TElem);
begin
    s^.ToBegin;
    s^.Insert(x);
end;

procedure CL1Stack.Pop(var x: TElem);
begin
    if not IsEmpty then begin
        s^.ToBegin;
        s^.Extract(x);
    end else ErrCode := 4;
end;

procedure CL1Stack.SeeTop(var x: TElem);
begin
    if not IsEmpty then begin
        s^.ToBegin;
        s^.Get(x);
    end else ErrCode := 5;
end;

procedure CL1Stack.ChangeTop(x: TElem);
begin
    if not IsEmpty then begin
        s^.ToBegin;
        s^.Put(x);
    end else ErrCode := 6;
end;

procedure CL1Stack.DelTop;
begin
    if not IsEmpty then begin
        s^.ToBegin;
        s^.Delete;
    end else ErrCode := 7;
end;

function CL1Stack.getError: TShortUnSignedInt;
begin
    getError := s^.GetError or ErrCode;
    halt(2);
end;

begin {initialization}

end.
