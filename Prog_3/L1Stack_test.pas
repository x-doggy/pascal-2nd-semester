{$mode objfpc}
{$R-}{$Q+}

program L1Stack_test;
uses crt, L1Stack_unit, L1Queue_unit;
const cAzure = 11;
      cGreen = 10;
      cMagenta = 13;
type TBool = 1..2;
     TMenuSelect = 1..9;
var s1, s2, s3, copy_s: PL1Stack;
    key, err: TMenuSelect;
    length1, length2, i: word;
    elem: TElem;
    f: file of TElem;
    fname: string;
    choose: TBool;

procedure InvertStack(var st: PL1Stack);
(*
* Процедура инвертирует стек через очередь
*)
var qu: PL1Queue; tmp: TElem;
begin
    if st<>NIL then begin
        new(qu, Init);
        while not st^.IsEmpty do begin
            st^.Pop(tmp);
            qu^.PushTail(tmp);
        end;
        while not qu^.IsEmpty do begin
            qu^.PopHead(tmp);
            st^.Push(tmp);
        end;
        dispose(PL1Queue(qu), Done);
        qu := NIL;
    end;
end;

procedure doAlgorythm(st1, st2: PL1Stack;
                      var st_res: PL1Stack;
                      var ce: TMenuSelect);
(*
* Процедура cоздаёт третий стек, состоящий из всех элементов
* первых двух, упорядоченных по невозрастанию.
* Первым и вторым параметром процедура принимает стеки,
* где st1 - первый стек, котороый упорядочен по невозрастанию.
* st2 - второй стек, который упорядочен по неубываанию.
* По ссылке возвращается st_res - результирующий стек.
* ce - код завершения процедуры:
    1 - стеки переданы непустые и верно упорядоченные
    2 - был передан пустой стек
    3 - какой-то стек передался неупорядоченным
    4 - стек вообще не создан.
*)
var tmp1, tmp2: TElem;
    len1, len2, length3: word;
    _s1, _s2: PL1Stack;
    isNonDec, isNonInc: boolean;
begin
    if (st1<>NIL) and (st2<>NIL) then begin
        length3 := 0;
        _s1 := NIL;
        _s2 := NIL;

        // проверка входных стеков на упорядоченность
        new(_s1, Copy(st1));
        new(_s2, Copy(st2));
        len1 := 0;
        len2 := 0;
        isNonDec := true;
        isNonInc := true;

        // проверяем на упорядоченность 1-ый стек
        _s1^.Pop(tmp1);
        _s1^.Pop(tmp2);
        while (not _s1^.IsEmpty) and isNonDec do begin
            if tmp1 < tmp2 then isNonDec := false else begin
                tmp1 := tmp2;
                _s1^.Pop(tmp2);
                inc(len1);
            end;
        end;

        // проверяем на упорядоченность 2-ой стек
        _s2^.Pop(tmp1);
        _s2^.Pop(tmp2);
        while (not _s2^.IsEmpty) and isNonInc do begin
            if tmp1 > tmp2 then isNonInc := false else begin
                tmp1 := tmp2;
                _s2^.Pop(tmp2);
                inc(len2);
            end;
        end;

        // здесь должна находится пара процедур очистки стеков _s1, _s2
        // я должен по идее их очистить для выполнения след. подзадачи,
        // однако вылетают ошибки и на удивление программа корректно
        // работает БЕЗ них!

        if isNonDec and isNonInc and (len1>0) and (len2>0) then begin
        // если стеки упорядоченны
            new(_s1, Copy(st1)); // создаём копии для выполнения задачи
            new(_s2, Copy(st2));
            InvertStack(_s1); // инвертируем для верного поэлементного сравнения

            _s1^.Pop(tmp1);
            _s2^.Pop(tmp2);
            while (not _s1^.IsEmpty) and (not _s2^.IsEmpty) do begin
                if tmp1 < tmp2 then begin
                    st_res^.Push(tmp1);
                    _s1^.Pop(tmp1);
                end else begin
                    st_res^.Push(tmp2);
                    _s2^.Pop(tmp2);
                end;
                inc(length3);
            end;

            // Упорядочили в новый массив всё, что самое большое по модулю.
            // Теперь нужно перекачать оставшиеся элементы стеков,
            // не попавшие под сравнение

            while not _s2^.IsEmpty do begin
                _s2^.Pop(tmp1);
                st_res^.Push(tmp1);
                inc(length3);
            end;
            while not _s1^.IsEmpty do begin
                _s1^.Pop(tmp1);
                st_res^.Push(tmp1);
                inc(length3);
            end;

            ce := 1;
        end else ce := 3;
    end else ce := 4;
end;

begin {main}

    s1 := NIL;
    s2 := NIL;
    s3 := NIL;
    copy_s := NIL;

    repeat
        textcolor(cAzure);
        clrscr;

        writeln('        (c) 2014, Владимир Стадник, 3-я прога');
        writeln('      Программа написана в Lubuntu 14.04, amd64');
        writeln(' Реализация стека через L1-список и выполнение алгоритма');
        writeln;
        writeln('            №6   Даны два стека целых чисел.');
        writeln('     Элементы в стеках упорядочены от дна к вершины');
        writeln('в первом стеке по невозрастанию, во втором - по неубыванию.');
        writeln('      Создать третий стек, состоящий из всех элементов');
        writeln('        первых двух, упорядоченных по невозрастанию.');
        writeln;
        if (s1=NIL) and (s2=NIL) then
            writeln('1. Создать стек')
        else
            writeln('1. Уничтожить стек');
        if (s1<>NIL) or (s2<>NIL) then begin
            writeln('2. Заполнить стек вручную');
            writeln('3. Заполнить стек псевдослучайно');
            writeln('4. Вывести стеки');
            textcolor(cGreen);
            writeln('5. Выполнить алгоритм');
            textcolor(cAzure);
        end;
        writeln('6. Загрузить стек в файл');
        writeln('7. Выгрузить стек из файла');
        writeln('9. Выйти из программы');
        writeln;
        write('Введите номер действия: ');
        textcolor(cMagenta);
        repeat
            {$I-} readln(key); {$I+}
        until IORESULT=0;
        textcolor(cAzure);

        case key of

            1:
            begin
                writeln('1. <Создание/удаление стека>');
                write('Номер стека [1, 2]: ');
                textcolor(cMagenta);
                repeat
                    {$I-} readln(choose); {$I+}
                until IORESULT=0;
                textcolor(cAzure);
                if choose=1 then begin
                    if s1=NIL then begin
                        new(s1, Init);
                        writeln('Стек #1 успешно создан!');
                    end else begin
                        dispose(PL1Stack(s1), Done);
                        s1 := NIL;
                        length1 := 0;
                        writeln('Стек #1 успешно очищен!');
                    end;
                end else begin
                    if s2=NIL then begin
                        new(s2, Init);
                        writeln('Стек #2 успешно создан!');
                    end else begin
                        dispose(PL1Stack(s2), Done);
                        s2 := NIL;
                        length2 := 0;
                        writeln('Стек #2 успешно очищен!');
                    end;
                end;
            end;

            2:
            begin
                writeln('2. <Заполнение вручную>');
                write('Номер стека для заполнения [1, 2]: ');
                textcolor(cMagenta);
                repeat
                    {$I-} readln(choose); {$I+}
                until IORESULT=0;
                textcolor(cAzure);
                write('#', choose, '  Введите кол-во эл-тов для заполнения: ');
                if choose=1 then begin
                    textcolor(cMagenta);
                    repeat
                        {$I-} readln(length1); {$I+}
                    until IORESULT=0;
                    textcolor(cAzure);
                    for i:=1 to length1 do begin
                        write('#1  Введите значение ', i:3, ' элемента: ');
                        textcolor(cMagenta);
                        repeat
                            {$I-} readln(elem); {$I+}
                        until IORESULT=0;
                        textcolor(cAzure);
                        s1^.Push(elem);
                    end;
                end else begin
                    textcolor(cMagenta);
                    repeat
                        {$I-} readln(length2); {$I+}
                    until IORESULT=0;
                    textcolor(cAzure);
                    for i:=1 to length2 do begin
                        write('#2  Введите значение ', i:3, ' элемента: ');
                        textcolor(cMagenta);
                        repeat
                            {$I-} readln(elem); {$I+}
                        until IORESULT=0;
                        textcolor(cAzure);
                        s2^.Push(elem);
                    end;
                end;
                writeln('Заполнено успешно!');
            end;

            3:
            begin
                writeln('3. <Заполнение псевдослучайно>');
                write('Номер стека для заполнения [1, 2]: ');
                textcolor(cMagenta);
                repeat
                    {$I-} readln(choose); {$I+}
                until IORESULT=0;
                textcolor(cAzure);
                randomize;
                write('#', choose, '  Введите кол-во эл-тов для заполнения: ');
                if choose=1 then begin
                    textcolor(cMagenta);
                    repeat
                        {$I-} readln(length1); {$I+}
                    until (IORESULT=0) and (length1>0);
                    textcolor(cAzure);
                    for i:=1 to length1 do begin
                        elem := random(200)-101;
                        s1^.Push(elem);
                        write(elem, ', ');
                    end;
                end else begin
                    textcolor(cMagenta);
                    repeat
                        {$I-} readln(length2); {$I+}
                    until (IORESULT=0) and (length2>0);
                    textcolor(cAzure);
                    for i:=1 to length2 do begin
                        elem := random(200)-101;
                        s2^.Push(elem);
                        write(elem, ', ');
                    end;
                end;
                writeln;
                writeln('Заполнено успешно!');
            end;

            4:
            begin
                writeln('4. <Просмотр стеков> [хвост->голова]');
                if (s1<>NIL) and (length1>0) then begin
                    new(copy_s, Copy(s1));
                    write('#1: ');
                    for i:=1 to length1 do begin
                        copy_s^.Pop(elem);
                        write(elem:4);
                    end;
                    dispose(PL1Stack(copy_s), Done);
                    copy_s := NIL;
                end else writeln('Стек #1 не создан!');
                writeln;
                if (s2<>NIL) and (length2>0) then begin
                    new(copy_s, Copy(s2));
                    write('#2: ');
                    for i:=1 to length2 do begin
                        copy_s^.Pop(elem);
                        write(elem:4);
                    end;
                    dispose(PL1Stack(copy_s), Done);
                    copy_s := NIL;
                end else writeln('Стек #2 не создан!');
            end;

            5:
            begin
                writeln('5. <Выполнение алгоритма>');
                new(s3, Init);
                if (s1<>NIL) and (s2<>NIL) then doAlgorythm(s1, s2, s3, err);
                writeln;
                case err of
                1:
                begin
                    // выводим стек на экран, если успешно
                    new(copy_s, Copy(s3));
                    while not copy_s^.IsEmpty do begin
                        copy_s^.Pop(elem);
                        write(elem, ', ');
                    end;
                    dispose(PL1Stack(copy_s), Done);
                    copy_s := NIL;
                    writeln;
                    writeln('Алгоритм выполнен успешно!');
                end;
                2: writeln('Был передан пустой стек!');
                3: writeln('Переданы неверно упорядоченные стеки!');
                4: writeln('Стек не создан');
                end;
            end;

            6:
            begin
                writeln('6. <Сохранение в файл>');
                write('Номер стека для сохранения [1, 2]: ');
                textcolor(cMagenta);
                repeat
                    {$I-} readln(choose); {$I+}
                until IORESULT=0;
                textcolor(cAzure);
                write('#', choose, '  Введите имя файла: ');
                textcolor(cMagenta);
                repeat
                    {$I-} readln(fname); {$I+}
                until IORESULT=0;
                textcolor(cAzure);
                if choose=1 then begin
                    new(copy_s, Copy(s1));
                    InvertStack(copy_s);
                    assign(f, fname);
                    {$I-} rewrite(f); {$I+}
                    if IORESULT<>0 then write('Ошибка открытия') else begin
                        for i:=1 to length1 do begin
                            copy_s^.Pop(elem);
                            {$I-} write(f, elem); {$I+}
                            if IORESULT<>0 then write('Ошибка записи');
                        end;
                    end;
                    {$I-} close(f); {$I+}
                    if IORESULT<>0 then write('Ошибка закрытия');
                end else begin
                    new(copy_s, Copy(s2));
                    InvertStack(copy_s);
                    assign(f, fname);
                    {$I-} rewrite(f); {$I+}
                    if IORESULT<>0 then write('Ошибка открытия') else begin
                        for i:=1 to length2 do begin
                            copy_s^.Pop(elem);
                            {$I-} write(f, elem); {$I+}
                            if IORESULT<>0 then write('Ошибка записи');
                        end;
                    end;
                    {$I-} close(f); {$I+}
                    if IORESULT<>0 then write('Ошибка закрытия');
                end;
                dispose(PL1Stack(copy_s), Done);
                copy_s := NIL;
                write('Записано успешно!');
            end;

            7:
            begin
                writeln('7. <Выгрузка из файла>');
                write('Номер стека для загрузки [1, 2]: ');
                textcolor(cMagenta);
                repeat
                    {$I-} readln(choose); {$I+}
                until IORESULT=0;
                textcolor(cAzure);
                write('#', choose, '  Введите имя файла: ');
                textcolor(cMagenta);
                repeat
                    {$I-} readln(fname); {$I+}
                until IORESULT=0;
                textcolor(cAzure);
                assign(f, fname);
                {$I-} reset(f); {$I+}
                if IORESULT<>0 then write('Ошибка: файл не найден!') else begin
                    {$I-} seek(f, 0); {$I+}
                    if IORESULT<>0 then write('Ошибка начала') else begin
                        if choose=1 then begin
                            if s1=NIL then begin
                                new(s1, Init);
                                writeln('Стек #1 успешно создан!');
                            end else begin
                                dispose(PL1Stack(s1), Done);
                                s1 := NIL;
                                writeln('Стек #1 был не пустым, при создании он очистился!');
                            end;
                            length1 := 0;
                            while not EOF(f) do begin
                                {$I-} read(f, elem); {$I+}
                                if IORESULT<>0 then write('Ошибка чтения') else begin
                                    s1^.Push(elem);
                                    inc(length1);
                                end;
                            end;
                        end else begin
                            if s2=NIL then begin
                                new(s2, Init);
                                writeln('Стек #2 успешно создан!');
                            end else begin
                                dispose(PL1Stack(s2), Done);
                                s2 := NIL;
                                writeln('Стек #2 был не пустым, при создании он очистился!');
                            end;
                            length2 := 0;
                            while not EOF(f) do begin
                                {$I-} read(f, elem); {$I+}
                                if IORESULT<>0 then write('Ошибка чтения') else begin
                                    s2^.Push(elem);
                                    inc(length2);
                                end;
                            end;
                        end;
                    end;
                end;
                {$I-} close(f); {$I+}
                if IORESULT<>0 then write('Ошибка закрытия');
            end;
        end;
        if key<>9 then readkey;
    until key=9;

    // забота о чистой памяти
    if s1<>NIL then begin
        dispose(PL1Stack(s1), Done);
        s1 := NIL;
        writeln('Стек #1 очищен!');
    end;
    if s2<>NIL then begin
        dispose(PL1Stack(s2), Done);
        s2 := NIL;
        writeln('Стек #2 очищен!');
    end;
    if s3<>NIL then begin
        dispose(PL1Stack(s3), Done);
        s3 := NIL;
        writeln('Стек #3 очищен!');
    end;
    if copy_s<>NIL then begin
        dispose(PL1Stack(copy_s), Done);
        copy_s := NIL;
        writeln('Вспомогательный стек очищен!');
    end;
end.
